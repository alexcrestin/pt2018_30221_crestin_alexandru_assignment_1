package default2;

import javax.swing.*;



import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.*;

public class Polinom {
    
   
    
    ArrayList<Monom> polinom= new ArrayList<Monom>();
    String rezultat="";
    
    
    public void afisare() {
    	rezultat="";
    	for(Monom i:polinom) {
    		if(i.getCoeficient() == 0) {
    			continue;
    		}
    			if(i.getCoeficient()>0) {
    		rezultat = rezultat + "+"+i.getCoeficient()+"x"+"^"+i.getExponent();}
    			else {
    				
    				rezultat = rezultat + i.getCoeficient()+"x"+"^"+i.getExponent();
    			}
    	
    	}
    	for(Monom i:polinom) {
    							//dupa fiecare afisare se reseteaza polinomul pt a nu influenta operatiile viitoare
    		i.setCoeficient(0);
    		i.setExponent(0);
    	}
    	
    	}
    
    public ArrayList<Monom> adunare(Polinom pol) {
		for(Monom j:pol.polinom) {
			for(Monom i:polinom) {
				if(i.getExponent() == j.getExponent()) {
					i.setCoeficient(i.getCoeficient() + j.getCoeficient());
					j.setCoeficient(0);
					j.setExponent(0);
				}
			}
			if (j.getCoeficient()!=0) {
				polinom.add(j);
			}
		}
		
		for(Monom i:polinom) {
			for(Monom j:polinom) {
				if(i!=j) {
					if(i.getExponent() == j.getExponent()) {
						i.setCoeficient(i.getCoeficient() + j.getCoeficient());
						j.setCoeficient(0);
						j.setExponent (0);
					}
				}
			}
		}
		return polinom;
	}
    
    
   
    public ArrayList<Monom> scadere(Polinom pol) {
		for(Monom j:pol.polinom) {
			for(Monom i:polinom) {
				if(i.getExponent() == j.getExponent()) {
					i.setCoeficient(i.getCoeficient() - j.getCoeficient());
					j.setCoeficient(0);
					j.setExponent(0);
				}
			}
			if (j.getCoeficient()!=0) {
				j.setCoeficient(-j.getCoeficient());
				polinom.add(j);
			}
		}
		
		for(Monom i:polinom) {
			for(Monom j:polinom) {
				if(i!=j) {
					if(i.getExponent() == j.getExponent()) {
						i.setCoeficient(i.getCoeficient() + j.getCoeficient());
						j.setCoeficient(0);
						j.setExponent (0);
					}
				}
			}
		}
		return polinom;
	}
    
    public ArrayList<Monom> inmultire(Polinom pol) {
    	
    	ArrayList<Monom> aux= new ArrayList<Monom>();
		
		
		for(Monom i:pol.polinom) {
			for(Monom j:polinom) {
				Monom m = new Monom();
				m.setCoeficient(i.getCoeficient()*j.getCoeficient());
				m.setExponent(i.getExponent() + j.getExponent());
				aux.add(m);
			}
		}
		for(Monom i:aux) {
			for(Monom j:aux) {
				if(i!=j) {
					if(i.getExponent() == j.getExponent()) {
						i.setCoeficient(i.getCoeficient() + j.getCoeficient());
						j.setCoeficient(0);
						j.setExponent (0);
					}
				}
			}
		}
		polinom=aux;
		return polinom;
	}
    
  
    
    public ArrayList<Monom> integrare() {
		for(Monom i:polinom) {
			i.setExponent(i.getExponent()+1);
			i.setCoeficient(i.getCoeficient()/i.getExponent());
		}
		for(Monom i:polinom) {
			for(Monom j:polinom) {
				if(i!=j) {
					if(i.getExponent() == j.getExponent()) {
						i.setCoeficient(i.getCoeficient() + j.getCoeficient());
						j.setCoeficient(0);
						j.setExponent (0);
					}
				}
			}
		}
		return polinom;
	}
    
   
    public ArrayList<Monom> derivare() {
		for(Monom i:polinom) {
			i.setCoeficient(i.getCoeficient()*i.getExponent());
			i.setExponent(i.getExponent() - 1);
		}
		for(Monom i:polinom) {
			for(Monom j:polinom) {
				if(i!=j) {
					if(i.getExponent() == j.getExponent()) {
						i.setCoeficient(i.getCoeficient() + j.getCoeficient());
						j.setCoeficient(0);
						j.setExponent (0);
					}
				}
			}
		}
		return polinom;
	}
    
    public int[] StrToVect(String rez) {
		String inlocuire = rez.replaceAll("[-]", "+-");
		String str = inlocuire.replaceAll("[A-Za-z+^]+", " ");
		String[] spliter = str.split(" ");
		
		int[] polinom = new int[spliter.length];
		for(int i = 0;i<spliter.length;i++) {
			polinom[i] = Integer.parseInt(spliter[i]);
		}
		return polinom;
		
	}  
 
    

}