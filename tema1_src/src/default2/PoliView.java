package default2;
import javax.swing.*;



import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;


public class PoliView  extends JFrame {
    
    private static final String INITIAL_VALUE = " ";
    
    
    JTextField t1= new JTextField();
	JTextField t2= new JTextField();
	JTextArea t4= new JTextArea();
	JButton b1= new JButton("Rezolva");
	
	
	String poli1="";
	String poli2="";
	
	Polinom p1 = new Polinom();
	Polinom p2 = new Polinom();
    
	
	
    PoliView(String titlu) {
    	
    	JComboBox c1= new JComboBox();
    	c1.addItem("Adunare");
    	c1.addItem("Scadere");
    	c1.addItem("Inmultire");
    	c1.addItem("Derivare");
    	c1.addItem("Integrare");
    	
    	     
        JPanel content = new JPanel();
        
        JLabel l1 = new JLabel ("P1:");
		JLabel l2 = new JLabel("P2:");
		
		l1.setSize(25,25);
		l1.setLocation(10,10);
		
		l2.setSize(25,25);
		l2.setLocation(10,40);
		
		t1.setSize(120,30);
		t1.setLocation(35,10);
		
		t2.setSize(120,30);
		t2.setLocation(35,40);
		
		c1.setSize(90,30);
		c1.setLocation(170,10);
		
		b1.setSize(120,30);
		b1.setLocation(10,80);
		
		t4.setSize(360,240);
		t4.setLocation(10,130);
		
		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent e) {
				
				poli1 = t1.getText();
				poli2 = t2.getText();
				
				String radical= (String)c1.getSelectedItem(); //ia ce e selectat in combo box
	
				int[] v1 = p1.StrToVect(poli1);
				int[] v2 = p1.StrToVect(poli2);
				
				for(int i = 0;i<v1.length;i=i+2) {
					Monom m = new Monom(v1[i],v1[i+1]);
					p1.polinom.add(m);
		
					
				}
				for(int i = 0;i<v2.length;i=i+2) {
					Monom m = new Monom(v2[i],v2[i+1]);
					p2.polinom.add(m);
					
				}
				
				if(radical.equals("Adunare")) {
				
				p1.adunare(p2);
					
				String orice="";
				 p1.afisare();
				 orice=p1.rezultat;
				
				t4.setText(orice);
				
				}
				else if(radical.equals("Scadere")){
					p1.scadere(p2);
						
					String orice="";
					 p1.afisare();
					 orice=p1.rezultat;
					
					t4.setText(orice);
				}else if(radical.equals("Inmultire")){
					p1.inmultire(p2);
					String orice="";
					 p1.afisare();
					 orice=p1.rezultat;
					
					t4.setText(orice);
					
				}else if(radical.equals("Derivare")){
					p1.derivare();
						
					String orice="";
					 p1.afisare();
					 orice=p1.rezultat;
					
					t4.setText(orice);
				}else if(radical.equals("Integrare")){
					p1.integrare();
				
					String orice="";
					 p1.afisare();
					 orice=p1.rezultat;
					
					t4.setText(orice);
				}
			
			}
		});
		
        content.setLayout(null);
        
        content.add(l1);
        content.add(l2);
        content.add(t1);
        content.add(t2);
        content.add(c1);
        content.add(b1);
        content.add(t4);
        
        
        this.setContentPane(content);
      
        this.setVisible(true);
        
        this.setTitle(titlu);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400,430);
    }
    
 
  
 	}