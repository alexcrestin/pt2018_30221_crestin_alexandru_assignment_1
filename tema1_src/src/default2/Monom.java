package default2;
import javax.swing.*;
import java.awt.*;

public class Monom {

	private int coeficient;
	private int exponent;
	
	public Monom() {
		this.coeficient=0;
		this.exponent=0;
	}
	
	public Monom(int coef, int exp) {
		
		this.coeficient = coef;
		this.exponent = exp;
		
	}

	public int getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}

	public int getExponent() {
		return exponent;
	}

	public void setExponent(int exponent) {
		this.exponent = exponent;
	}
	
	
	
	
	
}
